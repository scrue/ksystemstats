# Translation of ksysguard_plugins_global.po to Ukrainian
# Copyright (C) 2020-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard_plugins_global\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-12 00:56+0000\n"
"PO-Revision-Date: 2022-09-07 15:51+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: cpu/cpu.cpp:28 disks/disks.cpp:67 disks/disks.cpp:68 gpu/GpuDevice.cpp:20
#: power/power.cpp:39
#, kde-format
msgctxt "@title"
msgid "Name"
msgstr "Назва"

#: cpu/cpu.cpp:38
#, kde-format
msgctxt "@title"
msgid "Total Usage"
msgstr "Загальне використання"

#: cpu/cpu.cpp:39
#, kde-format
msgctxt "@title, Short for 'Total Usage'"
msgid "Usage"
msgstr "Використання"

#: cpu/cpu.cpp:45
#, kde-format
msgctxt "@title"
msgid "System Usage"
msgstr "Використання системою"

#: cpu/cpu.cpp:46
#, kde-format
msgctxt "@title, Short for 'System Usage'"
msgid "System"
msgstr "Система"

#: cpu/cpu.cpp:52
#, kde-format
msgctxt "@title"
msgid "User Usage"
msgstr "Використання користувачем"

#: cpu/cpu.cpp:53
#, kde-format
msgctxt "@title, Short for 'User Usage'"
msgid "User"
msgstr "Користувач"

#: cpu/cpu.cpp:59
#, kde-format
msgctxt "@title"
msgid "Wait Usage"
msgstr "Використання очікуванням"

#: cpu/cpu.cpp:60
#, kde-format
msgctxt "@title, Short for 'Wait Load'"
msgid "Wait"
msgstr "Очікування"

#: cpu/cpu.cpp:85
#, kde-format
msgctxt "@title"
msgid "Current Frequency"
msgstr "Поточна частота"

#: cpu/cpu.cpp:86
#, kde-format
msgctxt "@title, Short for 'Current Frequency'"
msgid "Frequency"
msgstr "Частота"

#: cpu/cpu.cpp:87
#, kde-format
msgctxt "@info"
msgid "Current frequency of the CPU"
msgstr "Поточна частота процесора"

#: cpu/cpu.cpp:92
#, kde-format
msgctxt "@title"
msgid "Current Temperature"
msgstr "Поточна температура"

#: cpu/cpu.cpp:93
#, kde-format
msgctxt "@title, Short for Current Temperatur"
msgid "Temperature"
msgstr "Температура"

#: cpu/cpu.cpp:100
#, kde-format
msgctxt "@title"
msgid "All"
msgstr "Усі"

#: cpu/cpu.cpp:119
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Frequency"
msgstr "Максимальна частота процесорів"

#: cpu/cpu.cpp:120
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Frequency'"
msgid "Max Frequency"
msgstr "Максимальна частота"

#: cpu/cpu.cpp:121
#, kde-format
msgctxt "@info"
msgid "Current maximum frequency between all CPUs"
msgstr "Поточна максимальна частота серед усіх процесорів"

#: cpu/cpu.cpp:126
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Frequency"
msgstr "Мінімальна частота процесорів"

#: cpu/cpu.cpp:127
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Frequency'"
msgid "Min Frequency"
msgstr "Мінімальна частота"

#: cpu/cpu.cpp:128
#, kde-format
msgctxt "@info"
msgid "Current minimum frequency between all CPUs"
msgstr "Поточна мінімальна частота серед усіх процесорів"

#: cpu/cpu.cpp:133
#, kde-format
msgctxt "@title"
msgid "Average CPU Frequency"
msgstr "Середня частота процесорів"

#: cpu/cpu.cpp:134
#, kde-format
msgctxt "@title, Short for 'Average CPU Frequency'"
msgid "Average Frequency"
msgstr "Середня частота"

#: cpu/cpu.cpp:135
#, kde-format
msgctxt "@info"
msgid "Current average frequency between all CPUs"
msgstr "Поточна середня частота серед усіх процесорів"

#: cpu/cpu.cpp:140
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Temperature"
msgstr "Максимальна температура процесорів"

#: cpu/cpu.cpp:141
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Temperature'"
msgid "Max Temperature"
msgstr "Максимальна температура"

#: cpu/cpu.cpp:147
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Temperature"
msgstr "Мінімальна температура процесорів"

#: cpu/cpu.cpp:148
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Temperature'"
msgid "Min Temperature"
msgstr "Мінімальна температура"

#: cpu/cpu.cpp:154
#, kde-format
msgctxt "@title"
msgid "Average CPU Temperature"
msgstr "Середня температура процесорів"

#: cpu/cpu.cpp:155
#, kde-format
msgctxt "@title, Short for 'Average CPU Temperature'"
msgid "Average Temperature"
msgstr "Середня температура"

#: cpu/cpu.cpp:171
#, kde-format
msgctxt "@title"
msgid "Number of CPUs"
msgstr "Кількість процесорів"

#: cpu/cpu.cpp:172
#, kde-format
msgctxt "@title, Short fort 'Number of CPUs'"
msgid "CPUs"
msgstr "Процесори"

#: cpu/cpu.cpp:173
#, kde-format
msgctxt "@info"
msgid "Number of physical CPUs installed in the system"
msgstr "Кількість фізичних процесорів, які встановлено у системі"

#: cpu/cpu.cpp:175
#, kde-format
msgctxt "@title"
msgid "Number of Cores"
msgstr "Кількість ядер"

#: cpu/cpu.cpp:176
#, kde-format
msgctxt "@title, Short fort 'Number of Cores'"
msgid "Cores"
msgstr "Ядра"

#: cpu/cpu.cpp:177
#, kde-format
msgctxt "@info"
msgid "Number of CPU cores across all physical CPUS"
msgstr "Кількість ядер процесорів для усіх фізичних процесорів"

#: cpu/cpuplugin.cpp:19
#, kde-format
msgid "CPUs"
msgstr "Процесори"

#: cpu/freebsdcpuplugin.cpp:151
#, kde-format
msgctxt "@title"
msgid "CPU %1"
msgstr "Процесор %1"

#: cpu/linuxcpuplugin.cpp:39
#, kde-format
msgctxt "@title"
msgid "Core %1"
msgstr "Ядро %1"

#: cpu/linuxcpuplugin.cpp:54
#, kde-format
msgctxt "@title"
msgid "CPU %1 Core %2"
msgstr "Процесор %1, ядро %2"

#: cpu/loadaverages.cpp:13
#, kde-format
msgctxt "@title"
msgid "Load Averages"
msgstr "Середнє завантаження"

#: cpu/loadaverages.cpp:14
#, kde-format
msgctxt "@title"
msgid "Load average (1 minute)"
msgstr "Середнє завантаження (1 хвилина)"

#: cpu/loadaverages.cpp:15
#, kde-format
msgctxt "@title"
msgid "Load average (5 minutes)"
msgstr "Середнє завантаження (5 хвилин)"

#: cpu/loadaverages.cpp:16
#, kde-format
msgctxt "@title"
msgid "Load average (15 minute)"
msgstr "Середнє завантаження (15 хвилин)"

#: cpu/loadaverages.cpp:18
#, kde-format
msgctxt "@title,  Short for 'Load average (1 minute)"
msgid "Load average (1m)"
msgstr "Середнє завантаження (1хв)"

#: cpu/loadaverages.cpp:19
#, kde-format
msgctxt "@title,  Short for 'Load average (5 minutes)"
msgid "Load average (5m)"
msgstr "Середнє завантаження (5хв)"

#: cpu/loadaverages.cpp:20
#, kde-format
msgctxt "@title,  Short for 'Load average (15 minutes)"
msgid "Load average (15m)"
msgstr "Середнє завантаження (15хв)"

#: cpu/loadaverages.cpp:22
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 1 minute"
msgstr "Кількість завдань у черзі запуску, усереднена за 1 хвилину"

#: cpu/loadaverages.cpp:23
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 5 minutes"
msgstr "Кількість завдань у черзі запуску, усереднена за 5 хвилин"

#: cpu/loadaverages.cpp:24
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 15 minutes"
msgstr "Кількість завдань у черзі запуску, усереднена за 15 хвилин"

#: disks/disks.cpp:71 disks/disks.cpp:234
#, kde-format
msgctxt "@title"
msgid "Total Space"
msgstr "Усього місця"

#: disks/disks.cpp:73 disks/disks.cpp:235
#, kde-format
msgctxt "@title Short for 'Total Space'"
msgid "Total"
msgstr "Загалом"

#: disks/disks.cpp:77 disks/disks.cpp:247
#, kde-format
msgctxt "@title"
msgid "Used Space"
msgstr "Використане місце"

#: disks/disks.cpp:79 disks/disks.cpp:248
#, kde-format
msgctxt "@title Short for 'Used Space'"
msgid "Used"
msgstr "Використано"

#: disks/disks.cpp:84 disks/disks.cpp:240
#, kde-format
msgctxt "@title"
msgid "Free Space"
msgstr "Вільне місце"

#: disks/disks.cpp:86 disks/disks.cpp:241
#, kde-format
msgctxt "@title Short for 'Free Space'"
msgid "Free"
msgstr "Вільно"

#: disks/disks.cpp:91 disks/disks.cpp:254
#, kde-format
msgctxt "@title"
msgid "Read Rate"
msgstr "Швидкість читання"

#: disks/disks.cpp:93 disks/disks.cpp:255
#, kde-format
msgctxt "@title Short for 'Read Rate'"
msgid "Read"
msgstr "Читання"

#: disks/disks.cpp:97 disks/disks.cpp:260
#, kde-format
msgctxt "@title"
msgid "Write Rate"
msgstr "Швидкість запису"

#: disks/disks.cpp:99 disks/disks.cpp:261
#, kde-format
msgctxt "@title Short for 'Write Rate'"
msgid "Write"
msgstr "Запис"

#: disks/disks.cpp:103 disks/disks.cpp:270
#, kde-format
msgctxt "@title"
msgid "Percentage Used"
msgstr "Частка використання"

#: disks/disks.cpp:107 disks/disks.cpp:266
#, kde-format
msgctxt "@title"
msgid "Percentage Free"
msgstr "Частка вільного"

#: disks/disks.cpp:140
#, kde-format
msgid "Disks"
msgstr "Диски"

#: disks/disks.cpp:232
#, kde-format
msgctxt "@title"
msgid "All Disks"
msgstr "Усі диски"

#: disks/disks.cpp:267
#, kde-format
msgctxt "@title, Short for `Percentage Free"
msgid "Free"
msgstr "Вільно"

#: disks/disks.cpp:271
#, kde-format
msgctxt "@title, Short for `Percentage Used"
msgid "Used"
msgstr "Використано"

#: gpu/AllGpus.cpp:14
#, kde-format
msgctxt "@title"
msgid "All GPUs"
msgstr "Усі ГП"

#: gpu/AllGpus.cpp:16
#, kde-format
msgctxt "@title"
msgid "All GPUs Usage"
msgstr "Використання усіх ГП"

#: gpu/AllGpus.cpp:17
#, kde-format
msgctxt "@title Short for 'All GPUs Usage'"
msgid "Usage"
msgstr "Використання"

#: gpu/AllGpus.cpp:26
#, kde-format
msgctxt "@title"
msgid "All GPUs Total Memory"
msgstr "Загальна пам'ять для усіх ГП"

#: gpu/AllGpus.cpp:27
#, kde-format
msgctxt "@title Short for 'All GPUs Total Memory'"
msgid "Total"
msgstr "Загалом"

#: gpu/AllGpus.cpp:31
#, kde-format
msgctxt "@title"
msgid "All GPUs Used Memory"
msgstr "Використана пам'ять для усіх ГП"

#: gpu/AllGpus.cpp:32
#, kde-format
msgctxt "@title Short for 'All GPUs Used Memory'"
msgid "Used"
msgstr "Використано"

#: gpu/GpuDevice.cpp:24
#, kde-format
msgctxt "@title"
msgid "Usage"
msgstr "Використання"

#: gpu/GpuDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "Total Video Memory"
msgstr "Загалом відеопам'яті"

#: gpu/GpuDevice.cpp:32
#, kde-format
msgctxt "@title Short for Total Video Memory"
msgid "Total"
msgstr "Загалом"

#: gpu/GpuDevice.cpp:35
#, kde-format
msgctxt "@title"
msgid "Video Memory Used"
msgstr "Використано відеопам'яті"

#: gpu/GpuDevice.cpp:37
#, kde-format
msgctxt "@title Short for Video Memory Used"
msgid "Used"
msgstr "Використано"

#: gpu/GpuDevice.cpp:41
#, kde-format
msgctxt "@title"
msgid "Frequency"
msgstr "Частота"

#: gpu/GpuDevice.cpp:45
#, kde-format
msgctxt "@title"
msgid "Memory Frequency"
msgstr "Частота пам'яті"

#: gpu/GpuDevice.cpp:49
#, kde-format
msgctxt "@title"
msgid "Temperature"
msgstr "Температура"

#: gpu/GpuDevice.cpp:53 power/power.cpp:106
#, kde-format
msgctxt "@title"
msgid "Power"
msgstr "Потужність"

#: gpu/GpuPlugin.cpp:31
#, kde-format
msgctxt "@title"
msgid "GPU"
msgstr "ГП"

#: gpu/LinuxBackend.cpp:54
#, kde-format
msgctxt "@title %1 is GPU number"
msgid "GPU %1"
msgstr "Гр. проц. %1"

#: lmsensors/lmsensors.cpp:22
#, kde-format
msgid "Hardware Sensors"
msgstr "Датчики обладнання"

#: memory/backend.cpp:17
#, kde-format
msgctxt "@title"
msgid "Physical Memory"
msgstr "Фізична пам'ять"

#: memory/backend.cpp:18
#, kde-format
msgctxt "@title"
msgid "Swap Memory"
msgstr "Пам'ять свопінгу"

#: memory/backend.cpp:39
#, kde-format
msgctxt "@title"
msgid "Total Physical Memory"
msgstr "Загалом фізичної пам'яті"

#: memory/backend.cpp:40
#, kde-format
msgctxt "@title, Short for 'Total Physical Memory'"
msgid "Total"
msgstr "Загалом"

#: memory/backend.cpp:44
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory"
msgstr "Використано фізичної пам'яті"

#: memory/backend.cpp:45
#, kde-format
msgctxt "@title, Short for 'Used Physical Memory'"
msgid "Used"
msgstr "Використано"

#: memory/backend.cpp:49
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory Percentage"
msgstr "Частка використаної фізичної пам'яті"

#: memory/backend.cpp:53
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory"
msgstr "Вільна фізична пам'ять"

#: memory/backend.cpp:54
#, kde-format
msgctxt "@title, Short for 'Free Physical Memory'"
msgid "Free"
msgstr "Вільно"

#: memory/backend.cpp:58
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory Percentage"
msgstr "Частка вільної фізичної пам'яті"

#: memory/backend.cpp:62
#, kde-format
msgctxt "@title"
msgid "Application Memory"
msgstr "Пам'ять програм"

#: memory/backend.cpp:63
#, kde-format
msgctxt "@title, Short for 'Application Memory'"
msgid "Application"
msgstr "Програма"

#: memory/backend.cpp:67
#, kde-format
msgctxt "@title"
msgid "Application Memory Percentage"
msgstr "Частка програм у пам'яті"

#: memory/backend.cpp:71
#, kde-format
msgctxt "@title"
msgid "Cache Memory"
msgstr "Пам'ять кешу"

#: memory/backend.cpp:72
#, kde-format
msgctxt "@title, Short for 'Cache Memory'"
msgid "Cache"
msgstr "Кеш"

#: memory/backend.cpp:76
#, kde-format
msgctxt "@title"
msgid "Cache Memory Percentage"
msgstr "Частка кешу у пам'яті"

#: memory/backend.cpp:80
#, kde-format
msgctxt "@title"
msgid "Buffer Memory"
msgstr "Пам'ять буфера"

#: memory/backend.cpp:81
#, kde-format
msgctxt "@title, Short for 'Buffer Memory'"
msgid "Buffer"
msgstr "Буфер"

#: memory/backend.cpp:82
#, kde-format
msgid "Amount of memory used for caching disk blocks"
msgstr "Обсяг пам'яті, який використано для кешування блоків на диску"

#: memory/backend.cpp:86
#, kde-format
msgctxt "@title"
msgid "Buffer Memory Percentage"
msgstr "Частка буфера у пам'яті"

#: memory/backend.cpp:90
#, kde-format
msgctxt "@title"
msgid "Total Swap Memory"
msgstr "Загалом резервної пам'яті"

#: memory/backend.cpp:91
#, kde-format
msgctxt "@title, Short for 'Total Swap Memory'"
msgid "Total"
msgstr "Загалом"

#: memory/backend.cpp:95
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory"
msgstr "Використана резервна пам'ять"

#: memory/backend.cpp:96
#, kde-format
msgctxt "@title, Short for 'Used Swap Memory'"
msgid "Used"
msgstr "Використано"

#: memory/backend.cpp:100
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory Percentage"
msgstr "Частка використаної резервної пам'яті"

#: memory/backend.cpp:104
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory"
msgstr "Вільна резервна пам'ять"

#: memory/backend.cpp:105
#, kde-format
msgctxt "@title, Short for 'Free Swap Memory'"
msgid "Free"
msgstr "Вільно"

#: memory/backend.cpp:109
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory Percentage"
msgstr "Частка вільної резервної пам'яті"

#: memory/memory.cpp:24
#, kde-format
msgctxt "@title"
msgid "Memory"
msgstr "Пам'ять"

#: network/AllDevicesObject.cpp:16
#, kde-format
msgctxt "@title"
msgid "All Network Devices"
msgstr "Усі пристрої мережі"

#: network/AllDevicesObject.cpp:18 network/AllDevicesObject.cpp:28
#: network/NetworkDevice.cpp:66 network/NetworkDevice.cpp:76
#, kde-format
msgctxt "@title"
msgid "Download Rate"
msgstr "Швидкість отримання"

#: network/AllDevicesObject.cpp:19 network/AllDevicesObject.cpp:29
#: network/NetworkDevice.cpp:67 network/NetworkDevice.cpp:77
#, kde-format
msgctxt "@title Short for Download Rate"
msgid "Download"
msgstr "Отримання"

#: network/AllDevicesObject.cpp:23 network/AllDevicesObject.cpp:33
#: network/NetworkDevice.cpp:71 network/NetworkDevice.cpp:81
#, kde-format
msgctxt "@title"
msgid "Upload Rate"
msgstr "Швидкість вивантаження"

#: network/AllDevicesObject.cpp:24 network/AllDevicesObject.cpp:34
#: network/NetworkDevice.cpp:72 network/NetworkDevice.cpp:82
#, kde-format
msgctxt "@title Short for Upload Rate"
msgid "Upload"
msgstr "Вивантаження"

#: network/AllDevicesObject.cpp:38 network/NetworkDevice.cpp:86
#, kde-format
msgctxt "@title"
msgid "Total Downloaded"
msgstr "Загалом отримано"

#: network/AllDevicesObject.cpp:39 network/NetworkDevice.cpp:87
#, kde-format
msgctxt "@title Short for Total Downloaded"
msgid "Downloaded"
msgstr "Отримано"

#: network/AllDevicesObject.cpp:43 network/NetworkDevice.cpp:91
#, kde-format
msgctxt "@title"
msgid "Total Uploaded"
msgstr "Загалом вивантажено"

#: network/AllDevicesObject.cpp:44 network/NetworkDevice.cpp:92
#, kde-format
msgctxt "@title Short for Total Uploaded"
msgid "Uploaded"
msgstr "Вивантажено"

#: network/NetworkDevice.cpp:15
#, kde-format
msgctxt "@title"
msgid "Network Name"
msgstr "Назва мережі"

#: network/NetworkDevice.cpp:16
#, kde-format
msgctxt "@title Short of Network Name"
msgid "Name"
msgstr "Назва"

#: network/NetworkDevice.cpp:19
#, kde-format
msgctxt "@title"
msgid "Signal Strength"
msgstr "Потужність сигналу"

#: network/NetworkDevice.cpp:20
#, kde-format
msgctxt "@title Short of Signal Strength"
msgid "Signal"
msgstr "Сигнал"

#: network/NetworkDevice.cpp:26
#, kde-format
msgctxt "@title"
msgid "IPv4 Address"
msgstr "Адреса IPv4"

#: network/NetworkDevice.cpp:27
#, kde-format
msgctxt "@title Short of IPv4 Address"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "IPv4 Gateway"
msgstr "Шлюз IPv4"

#: network/NetworkDevice.cpp:31
#, kde-format
msgctxt "@title Short of IPv4 Gateway"
msgid "IPv4 Gateway"
msgstr "Шлюз IPv4"

#: network/NetworkDevice.cpp:34
#, kde-format
msgctxt "@title"
msgid "IPv4 Subnet Mask"
msgstr "Маска підмережі IPv4"

#: network/NetworkDevice.cpp:35
#, kde-format
msgctxt "@title Short of IPv4 Subnet Mask"
msgid "IPv4 Subnet Mask"
msgstr "Маска підмережі IPv4"

#: network/NetworkDevice.cpp:38
#, kde-format
msgctxt "@title"
msgid "IPv4 with Prefix Length"
msgstr "IPv4 із довжиною префікса"

#: network/NetworkDevice.cpp:39
#, kde-format
msgctxt "@title Short of IPv4 Prefix Length"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:42
#, kde-format
msgctxt "@title"
msgid "IPv4 DNS"
msgstr "DNS IPv4"

#: network/NetworkDevice.cpp:43
#, kde-format
msgctxt "@title Short of IPv4 DNS"
msgid "IPv4 DNS"
msgstr "DNS IPv4"

#: network/NetworkDevice.cpp:46
#, kde-format
msgctxt "@title"
msgid "IPv6 Address"
msgstr "Адреса IPv6"

#: network/NetworkDevice.cpp:47
#, kde-format
msgctxt "@title Short of IPv6 Address"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:50
#, kde-format
msgctxt "@title"
msgid "IPv6 Gateway"
msgstr "Шлюз IPv6"

#: network/NetworkDevice.cpp:51
#, kde-format
msgctxt "@title Short of IPv6 Gateway"
msgid "IPv6 Gateway"
msgstr "Шлюз IPv6"

#: network/NetworkDevice.cpp:54
#, kde-format
msgctxt "@title"
msgid "IPv6 Subnet Mask"
msgstr "Маска підмережі IPv6"

#: network/NetworkDevice.cpp:55
#, kde-format
msgctxt "@title Short of IPv6 Subnet Mask"
msgid "IPv6 Subnet Mask"
msgstr "Маска підмережі IPv6"

#: network/NetworkDevice.cpp:58
#, kde-format
msgctxt "@title"
msgid "IPv6 with Prefix Length"
msgstr "IPv6 із довжиною префікса"

#: network/NetworkDevice.cpp:59
#, kde-format
msgctxt "@title Short of IPv6 Prefix Length"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:62
#, kde-format
msgctxt "@title"
msgid "IPv6 DNS"
msgstr "DNS IPv6"

#: network/NetworkDevice.cpp:63
#, kde-format
msgctxt "@title Short of IPv6 DNS"
msgid "IPv6 DNS"
msgstr "DNS IPv6"

#: network/NetworkPlugin.cpp:43
#, kde-format
msgctxt "@title"
msgid "Network Devices"
msgstr "Пристрої мережі"

#: osinfo/osinfo.cpp:106
#, kde-format
msgctxt "@title"
msgid "Operating System"
msgstr "Операційна система"

#: osinfo/osinfo.cpp:108
#, kde-format
msgctxt "@title"
msgid "Kernel"
msgstr "Ядро"

#: osinfo/osinfo.cpp:109
#, kde-format
msgctxt "@title"
msgid "Kernel Name"
msgstr "Назва ядра"

#: osinfo/osinfo.cpp:110
#, kde-format
msgctxt "@title"
msgid "Kernel Version"
msgstr "Версія ядра"

#: osinfo/osinfo.cpp:111
#, kde-format
msgctxt "@title"
msgid "Kernel Name and Version"
msgstr "Назва і версія ядра"

#: osinfo/osinfo.cpp:112
#, kde-format
msgctxt "@title Kernel Name and Version"
msgid "Kernel"
msgstr "Ядро"

#: osinfo/osinfo.cpp:114
#, kde-format
msgctxt "@title"
msgid "System"
msgstr "Система"

#: osinfo/osinfo.cpp:115
#, kde-format
msgctxt "@title"
msgid "Hostname"
msgstr "Назва вузла"

#: osinfo/osinfo.cpp:116
#, kde-format
msgctxt "@title"
msgid "Operating System Name"
msgstr "Назва операційної системи"

#: osinfo/osinfo.cpp:117
#, kde-format
msgctxt "@title"
msgid "Operating System Version"
msgstr "Версія операційної системи"

#: osinfo/osinfo.cpp:118
#, kde-format
msgctxt "@title"
msgid "Operating System Name and Version"
msgstr "Назва і версія операційної системи"

#: osinfo/osinfo.cpp:119
#, kde-format
msgctxt "@title Operating System Name and Version"
msgid "OS"
msgstr "ОС"

#: osinfo/osinfo.cpp:120
#, kde-format
msgctxt "@title"
msgid "Operating System Logo"
msgstr "Логотип операційної системи"

#: osinfo/osinfo.cpp:121
#, kde-format
msgctxt "@title"
msgid "Operating System URL"
msgstr "Адреса сайта операційної системи"

#: osinfo/osinfo.cpp:122
#, kde-format
msgctxt "@title"
msgid "Uptime"
msgstr "Тривалість роботи"

#: osinfo/osinfo.cpp:125
#, kde-format
msgctxt "@title"
msgid "KDE Plasma"
msgstr "Плазма KDE"

#: osinfo/osinfo.cpp:126
#, kde-format
msgctxt "@title"
msgid "Qt Version"
msgstr "Версія Qt"

#: osinfo/osinfo.cpp:127
#, kde-format
msgctxt "@title"
msgid "KDE Frameworks Version"
msgstr "Версія KDE Frameworks"

#: osinfo/osinfo.cpp:128
#, kde-format
msgctxt "@title"
msgid "KDE Plasma Version"
msgstr "Версія Плазми KDE"

#: osinfo/osinfo.cpp:129
#, kde-format
msgctxt "@title"
msgid "Window System"
msgstr "Система роботи з вікнами"

#: osinfo/osinfo.cpp:163
#, kde-format
msgctxt "@info"
msgid "Unknown"
msgstr "Невідома"

#: power/power.cpp:44 power/power.cpp:45
#, kde-format
msgctxt "@title"
msgid "Design Capacity"
msgstr "Проєктна місткість"

#: power/power.cpp:47
#, kde-format
msgid "Amount of energy that the Battery was designed to hold"
msgstr "Запас енергії, який теоретично здатен зберігати акумулятор"

#: power/power.cpp:53 power/power.cpp:54 power/power.cpp:72
#, kde-format
msgctxt "@title"
msgid "Current Capacity"
msgstr "Поточна місткість"

#: power/power.cpp:56
#, kde-format
msgid "Amount of energy that the battery can currently hold"
msgstr "Запас енергії, який зараз може зберігати акумулятор"

#: power/power.cpp:62 power/power.cpp:63
#, kde-format
msgctxt "@title"
msgid "Health"
msgstr "Стан"

#: power/power.cpp:65
#, kde-format
msgid "Percentage of the design capacity that the battery can hold"
msgstr "Частка проєктної місткості, з якою може працювати акумулятор"

#: power/power.cpp:71
#, kde-format
msgctxt "@title"
msgid "Charge"
msgstr "Заряд"

#: power/power.cpp:74
#, kde-format
msgid "Amount of energy that the battery is currently holding"
msgstr "Запас енергії, який зараз зберігає акумулятор"

#: power/power.cpp:80 power/power.cpp:81
#, kde-format
msgctxt "@title"
msgid "Charge Percentage"
msgstr "Відсоток заряду"

#: power/power.cpp:83
#, kde-format
msgid ""
"Percentage of the current capacity that the battery is currently holding"
msgstr "Частка поточної місткості, на яку зараз заряджено акумулятор"

#: power/power.cpp:90
#, kde-format
msgctxt "@title"
msgid "Charging Rate"
msgstr "Швидкість заряджання"

#: power/power.cpp:91
#, kde-format
msgctxt "@title"
msgid "Charging  Rate"
msgstr "Швидкість заряджання"

#: power/power.cpp:93
#, kde-format
msgid ""
"Power that the battery is being charged with (positive) or discharged "
"(negative)"
msgstr ""
"Швидкість, із якою заряджається (додатна) або розряджається (від'ємна) "
"акумулятор"

#~ msgctxt "@title %1 is a number"
#~ msgid "Voltage %1"
#~ msgstr "Напруга %1"

#~ msgctxt "@title %1 is a number"
#~ msgid "Fan %1"
#~ msgstr "Вентилятор %1"

#~ msgctxt "@title %1 is a number"
#~ msgid "Temperature %1"
#~ msgstr "Температура %1"

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by applications."
#~ msgstr "Частка пам'яті, яку займають програми."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the buffer."
#~ msgstr "Частка пам'яті, яку займає буфер"

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the cache."
#~ msgstr "Частка пам'яті, яку займає кеш."

#~ msgctxt "@title Free Memory Percentage"
#~ msgid "Free"
#~ msgstr "Вільно"

#~ msgctxt "@info"
#~ msgid "Percentage of free memory."
#~ msgstr "Частка вільної пам'яті."

#~ msgctxt "@title Used Memory Percentage"
#~ msgid "Used"
#~ msgstr "Використано"

#~ msgctxt "@info"
#~ msgid "Percentage of used memory."
#~ msgstr "Частка використаної пам'яті."

#~ msgctxt "@title"
#~ msgid "Available Memory Percentage"
#~ msgstr "Частка доступної пам'яті"

#~ msgctxt "@title Available Memory Percentage"
#~ msgid "Available"
#~ msgstr "Доступно"

#~ msgctxt "@info"
#~ msgid "Percentage of available memory."
#~ msgstr "Частка доступної пам'яті."

#~ msgctxt "@title"
#~ msgid "Allocated Memory Percentage"
#~ msgstr "Частка розподіленої пам'яті"

#~ msgctxt "@title Allocated Memory Percentage"
#~ msgid "Allocated"
#~ msgstr "Розподілено"

#~ msgctxt "@info"
#~ msgid "Percentage of allocated memory."
#~ msgstr "Частка розподіленої пам'яті."

#~ msgctxt "@title Total CPU Usage"
#~ msgid "Usage"
#~ msgstr "Використання"

#~ msgctxt "@title Total Memory Usage"
#~ msgid "Total Used"
#~ msgstr "Загалом використано"

#~ msgctxt "@title Cached Memory Usage"
#~ msgid "Cached"
#~ msgstr "Кешовано"

#~ msgctxt "@title Free Memory Amount"
#~ msgid "Free"
#~ msgstr "Вільно"

#~ msgctxt "@title Available Memory Amount"
#~ msgid "Available"
#~ msgstr "Доступно"

#~ msgctxt "@title Application Memory Usage"
#~ msgid "Application"
#~ msgstr "Програма"

#~ msgctxt "@title Buffer Memory Usage"
#~ msgid "Buffer"
#~ msgstr "Буфер"

#~ msgctxt "@title Number of Processors"
#~ msgid "Processors"
#~ msgstr "Процесори"

#~ msgctxt "@title Number of Cores"
#~ msgid "Cores"
#~ msgstr "Ядра"

#~ msgctxt "@title"
#~ msgid "GPU %1 Power Usage"
#~ msgstr "Спож. ен. гр. проц. %1"

#~ msgctxt "@title GPU Power Usage"
#~ msgid "Power"
#~ msgstr "Споживання"

#~ msgctxt "@title GPU Temperature"
#~ msgid "Temperature"
#~ msgstr "Температура"

#~ msgctxt "@title"
#~ msgid "GPU %1 Shared Memory Usage"
#~ msgstr "Вик. спільної пам. граф. проц. %1"

#~ msgctxt "@title"
#~ msgid "GPU %1 Encoder Usage"
#~ msgstr "Вик. кодувальника гр. проц. %1"

#~ msgctxt "@title GPU Encoder Usage"
#~ msgid "Encoder"
#~ msgstr "Кодувальник"

#~ msgctxt "@title"
#~ msgid "GPU %1 Decoder Usage"
#~ msgstr "Вик. декодера граф. проц. %1"

#~ msgctxt "@title GPU Decoder Usage"
#~ msgid "Decoder"
#~ msgstr "Декодер"

#~ msgctxt "@title"
#~ msgid "GPU %1 Memory Clock"
#~ msgstr "Год. пам'яті граф. проц. %1"

#~ msgctxt "@title GPU Memory Clock"
#~ msgid "Memory Clock"
#~ msgstr "Годинник пам'яті"

#~ msgctxt "@title"
#~ msgid "GPU %1 Processor Clock"
#~ msgstr "Годинник граф. проц. %1"

#~ msgctxt "@title GPU Processor Clock"
#~ msgid "Processor Clock"
#~ msgstr "Годинник процесора"

#~ msgctxt "@title NVidia GPU information"
#~ msgid "NVidia"
#~ msgstr "NVidia"

#~ msgctxt "@title"
#~ msgid "Disk Read Accesses"
#~ msgstr "Звернень читання з дисків"

#~ msgctxt "@info"
#~ msgid "Read accesses across all disk devices"
#~ msgstr "Кількість звернень для читання на усіх дискових пристроях"

#~ msgctxt "@title"
#~ msgid "Disk Write Accesses"
#~ msgstr "Звернень запису на диски"

#~ msgctxt "@info"
#~ msgid "Write accesses across all disk devices"
#~ msgstr "Кількість звернень для запису на усіх дискових пристроях"

#~ msgctxt "@title All Network Interfaces"
#~ msgid "All"
#~ msgstr "Усі"

#~ msgctxt "@title"
#~ msgid "Received Data Rate"
#~ msgstr "Швидкість отримання даних"

#~ msgctxt "@info"
#~ msgid "The rate at which data is received on all interfaces."
#~ msgstr "Швидкість, з якою отримуються дані на усіх інтерфейсах."

#~ msgctxt "@title"
#~ msgid "Total Received Data"
#~ msgstr "Загалом отриманих даних"

#~ msgctxt "@info"
#~ msgid "The total amount of data received on all interfaces."
#~ msgstr "Загальний об'єм даних, які отримано на усіх інтерфейсах."

#~ msgctxt "@title"
#~ msgid "Sent Data Rate"
#~ msgstr "Швидкість надсилання даних"

#~ msgctxt "@title Sent Data Rate"
#~ msgid "Up"
#~ msgstr "Надсилання"

#~ msgctxt "@info"
#~ msgid "The rate at which data is sent on all interfaces."
#~ msgstr "Швидкість, з якою надсилаються дані на усіх інтерфейсах."

#~ msgctxt "@title"
#~ msgid "Total Sent Data"
#~ msgstr "Загалом надісланих даних"

#~ msgctxt "@info"
#~ msgid "The total amount of data sent on all interfaces."
#~ msgstr "Загальний об'єм надісланих даних на усіх інтерфейсах."

#~ msgctxt "@title Allocated Memory Percentage"
#~ msgid "Used"
#~ msgstr "Використано"

#~ msgctxt "@title Available Memory Amount"
#~ msgid "Avalable"
#~ msgstr "Доступно"
